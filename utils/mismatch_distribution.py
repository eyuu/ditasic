#!/usr/bin/env python

import argparse
import pysam
import numpy as np
import subprocess
import glob, os
from Bio import SeqIO

parser = argparse.ArgumentParser()
parser.add_argument("-n", type=int, help="Number of reads to sample for mismatch parameter test.")
parser.add_argument("refs_path", help="Absolute paths to the reference genomes the sample will be mapped to.")
parser.add_argument("sample", help="Sample with similar characteristics.")

args = parser.parse_args()

refs = [line.strip() for line in open(args.refs_path)]

# concatenate refs and build bowtie2 index
print("Building Bowtie2 index..")
with open('tmp_ref.fasta', 'w') as outfile:
    for in_path in refs:
        with open(in_path) as in_ref:
            for line in in_ref:
                outfile.write(line)

FNULL = open(os.devnull, 'w')
subprocess.call('bowtie2-build tmp_ref.fasta tmp_index', shell=True, stdout=FNULL)

# count the of sequences in the sample (to select sequences to sample)
# (could use reservoir sampling too, but probably overkill)
n = sum(1 for record in SeqIO.parse(args.sample, "fastq"))
sample_count = min(args.n, n)
print("Sampling %i (of %i) sequences from %s .."%(args.n, n, args.sample))

# select the sequences to sample
sample_index = np.sort(np.random.choice(n, size=sample_count, replace=False))
sample_pos   = 0
sample_name  = "subsample_%i.fastq"%args.n

# write sampled sequences to new file
with open(sample_name, "w") as output_sample:
    for i,record in enumerate(SeqIO.parse(args.sample, "fastq")):
        if (sample_pos < sample_count) and (i == sample_index[sample_pos]):
            SeqIO.write(record, output_sample, "fastq")
            sample_pos += 1

# map sample to concatenated index 
print("Mapping and analyzing sample..")
subprocess.call('bowtie2 -x tmp_index -U %s -S tmp_out.sam'%sample_name, shell=True) 

# analyze resulting sam file
input_sam = pysam.AlignmentFile("tmp_out.sam")

# mismatch counts for reads mapped to reference i, i=1..N
mismatches = []
lengths = []

for record in input_sam:
    if not (record.is_secondary or record.is_unmapped):
        mismatches.append(record.get_tag('XM'))
        lengths.append(len(record.seq))

avg_len = np.mean(np.array(lengths))
avg_mm  = np.mean(np.array(mismatches, dtype=np.float32))/avg_len

# just output average mismatch probabilities and suggested Mason parameters. 
print("Mason mismatch probabilities: Begin %f, Avg %f, End %f"%(avg_mm/2, avg_mm, 3*avg_mm))

# clean up 
os.remove('tmp_ref.fasta')
os.remove('tmp_out.sam')
for f in glob.glob("tmp_index*bt2"):
    os.remove(f)
